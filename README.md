**# README #**

Онлайн калькулятор страховки ОСАГО ЧАО "СК "Украинский страховой стандарт".
Публичная демонстрационная версия.
Readme ver.1.2

**### Описание ###**

Программа умеет:
1)утём доступа на выделенный web-ресурс проводить калькуляцию цены полиса ОСАГО по стандартам ЧАО "СК "Украинский страховой стандарт", действующим на момент написания данной программы;

2)Доставлять online-заявку расчитанного полиса ОСАГО менеджеру путём отправки эл.письма через указанный smtp-сервер;

3)Контролировать менеджеру поступившие онлайн-заявки из админ-части.

4)Менять значения коэффициентов ОСАГО в админ-части.

Калькулятор работает в стеке java-springBoot-springMVC-JPA-springREST-springSecurity-thymeleaf-bootstrap-javaQuery.
База данных - Postgresql.
Расчёт происходит на стороне сервера, браузер пользователя лишь получает валидные данные и формы для выбора.

**### Как пользоваться ###**
Http:
/
Интерфейс калькулятора;

/login
Логин для просмотра имеющихся заявок;

API:
/api/calculator/osago/1.0/public/getcities
Возвращает список городов;

/api/calculator/osago/1.0/public/calculate
Возвращает результат калькуляции, соотвествующий параметрам, прилагаемым в теле запроса.
Требует параметры: k1,k2,k3,k3b,k3a,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK;

/api/calculator/osago/1.0/public/neworder
Принимает данные форм и добавляет в базу новую заявку на ОСАГО.
Требует параметры: k1,k2,k3,k3b,k3a,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,inputSecondName,inputFirstName,inputPatronymic,inputTelephone,inputEmail;

/api/calculator/osago/1.0/private/manager/order/getall
Отдаёт список всех заявок (в упрощённом виде) на ОСАГО в виде json объекта, требует авторизации;

/api/calculator/osago/1.0/private/manager/order/get/{id}
Принимает id заявки на ОСАГО и отдаёт подробности по ней в виде json объекта, требует авторизации;

/api/calculator/osago/1.0/private/manager/order/changestatus/{id}
Принимает id заявки на ОСАГО и меняет её поля: status и isProcessed, требует авторизации;

/api/calculator/osago/1.0/private/admin/coefficient/getall
Отдаёт список всех коэффициентов, требует авторизации;

/api/calculator/osago/1.0/private/admin/coefficient/edit
Изменяет существующий коэффициент либо создаёт новый, требует авторизации.
Требует параметры: coefficientId(String), coefficientGroup(String), coefficientValue(Double), description(String), sortingVal(int);

/api/calculator/osago/1.0/private/admin/coefficient/delete
Удаляет коэффициент, требует авторизации.
Требует параметры: coefficientId(String);

**### Системные требования ###**

Java 8
Postgresql

**### Как разворачивать ###**

Программа представляет собой толстый java-архив jar, с интегрированным контейнером сервлетов Tomcat. Для базового запуска достаточно произвести его запуск в вирт.машине Java.

**### автор ###**

* Сорокин С.И. sorokin1984@gmail.com