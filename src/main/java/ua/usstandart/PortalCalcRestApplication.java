package ua.usstandart;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class PortalCalcRestApplication {

	private static String VERSION;
	private static String ARTIFACTID;

	public static void main(String[] args) {
		SpringApplication.run(PortalCalcRestApplication.class, args);
		System.out.println("Server "+ARTIFACTID+" ver: "+VERSION+" started "+ new Date(System.currentTimeMillis()));
	}

	@Value( "${version}" )
	public void setVersion(String ver) {
		VERSION = ver;
	}

	@Value( "${artifactId}" )
	public void setArtifactId(String id) {
		ARTIFACTID = id;
	}
}