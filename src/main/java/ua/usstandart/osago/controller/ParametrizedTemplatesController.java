package ua.usstandart.osago.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ua.usstandart.osago.persistence.model.OsagoCalcOrder;
import ua.usstandart.osago.persistence.model.OsagoCoefficients;
import ua.usstandart.osago.service.impl.OsagoCalcOrderServiceImpl;
import ua.usstandart.osago.service.impl.OsagoCalcServiceImpl;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * расширенный web контроллер ОСАГО калькулятора, использует thymeleaf шаблонизатор, для запросов с параметрами
 */

@Controller
public class ParametrizedTemplatesController {

    @Autowired
    private OsagoCalcOrderServiceImpl osagoCalcOrderService;
    @Autowired
    private OsagoCalcServiceImpl osagoCalcService;

    @RequestMapping("/cabinet/orders-osago/get/{id}")
    public Object osagoOrderDetails(@PathVariable(value = "id") long id, Model model) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            OsagoCalcOrder order = osagoCalcOrderService.getById(id);

            model.addAttribute("id", order.getId());
            model.addAttribute("k1Value", order.getK1Value());
            model.addAttribute("k1Description", osagoCalcService.getByCoefficientId(order.getK1Id()).getDescription());
            model.addAttribute("k2Value", order.getK2Value());
            model.addAttribute("k2Description", osagoCalcService.getByCoefficientId(order.getK2Id()).getDescription());
            model.addAttribute("k3Value", order.getK3Value());
            model.addAttribute("k3aDescription", osagoCalcService.getByCoefficientId(order.getK3aId()).getDescription());
            model.addAttribute("k3bDescription", osagoCalcService.getByCoefficientId(order.getK3bId()).getDescription());
            model.addAttribute("k4Value", order.getK4Value());
            model.addAttribute("k4Description", osagoCalcService.getByCoefficientId(order.getK4Id()).getDescription());
            model.addAttribute("k5Value", order.getK5Value());
            model.addAttribute("k5Description", osagoCalcService.getByCoefficientId(order.getK5Id()).getDescription());
            model.addAttribute("k6Value", order.getK6Value());
            model.addAttribute("k6Description", osagoCalcService.getByCoefficientId(order.getK6Id()).getDescription());
            model.addAttribute("k7Value", order.getK7Value());
            model.addAttribute("k7Description", osagoCalcService.getByCoefficientId(order.getK7Id()).getDescription());
            model.addAttribute("kParkValue", order.getkParkValue());
            model.addAttribute("kParkDescription", osagoCalcService.getByCoefficientId(order.getkParkId()).getDescription());
            model.addAttribute("kBMValue", order.getkBMValue());
            model.addAttribute("kBMDescription", osagoCalcService.getByCoefficientId(order.getkBMId()).getDescription());
            model.addAttribute("kPrivilegesValue", order.getkPrivilegesValue());
            model.addAttribute("kPrivilegesDescription", osagoCalcService.getByCoefficientId(order.getkPrivilegesId()).getDescription());
            model.addAttribute("kFranshizaDescription", osagoCalcService.getByCoefficientId(order.getkFranshizaId()).getDescription());
            model.addAttribute("kOTKDescription", osagoCalcService.getByCoefficientId(order.getNeedOTKId()).getDescription());
            model.addAttribute("kOTKDate", formatter.format(order.getkOTKDate()));
            model.addAttribute("kResultValue", order.getResultValue());
            model.addAttribute("inputSecondName", order.getInputSecondName());
            model.addAttribute("inputFirstName", order.getInputFirstName());
            model.addAttribute("inputPatronymic", order.getInputPatronymic());
            model.addAttribute("inputTelephone", order.getInputTelephone());
            model.addAttribute("inputEmail", order.getInputEmail());
            model.addAttribute("inputDate", order.getDateAdded().toString());
            model.addAttribute("isProcessed", order.getIsprocessed());
            model.addAttribute("status", order.getStatus());

            return "osagoOrderDetails";
        }

        catch (NullPointerException ex){
            return "error";
        }

    }

    @RequestMapping("/cabinet/orders-osago")
    public Object osagoOrders(Model model, @RequestParam(value="isProcessed", defaultValue="false") boolean isProcessed){
        model.addAttribute("osagoOrders",osagoCalcOrderService.getSimpleListOfOrders(isProcessed));
        return "osagoOrders";
    }

    @RequestMapping("/cabinet/coefficients-osago")
    public Object editOsagoCoeficients(Model model){
        List<OsagoCoefficients>coefficients = osagoCalcService.getAll();
        model.addAttribute("coefficients", coefficients);
        return "coefficientsOsago";
    }

    @RequestMapping("/cabinet/coefficients-osago/edit")
    public Object editOsagoCoeficients(@RequestParam(value = "coefficientId")String coefficientId, Model model){
        OsagoCoefficients coefficient = osagoCalcService.getByCoefficientId(coefficientId);
        model.addAttribute("coefficientId", coefficient.getCoefficientId());
        model.addAttribute("coefficientGroup", coefficient.getCoefficientGroup());
        model.addAttribute("coefficientValue", coefficient.getCoefficientValue());
        model.addAttribute("description", coefficient.getDescription());
        model.addAttribute("sortingVal", coefficient.getSortingVal());
        return "coefficientsOsagoEdit";
    }

    @RequestMapping("/cabinet/coefficients-osago/new")
    public Object newOsagoCoeficients(Model model){
        model.addAttribute("coefficientId", "потрібно заповнити");
        model.addAttribute("coefficientGroup", "потрібно заповнити");
        model.addAttribute("coefficientValue", "потрібно заповнити");
        model.addAttribute("description", "потрібно заповнити");
        model.addAttribute("sortingVal", "1");
        return "coefficientsOsagoEdit";
    }
}
