package ua.usstandart.osago.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ua.usstandart.osago.enums.OsagoOrderStatus;
import ua.usstandart.osago.persistence.dao.RoleRepository;
import ua.usstandart.osago.persistence.dao.UserRepository;
import ua.usstandart.osago.persistence.model.OsagoCalcOrder;
import ua.usstandart.osago.persistence.model.OsagoCoefficients;
import ua.usstandart.osago.persistence.model.User;
import ua.usstandart.osago.service.impl.*;
import ua.usstandart.osago.validator.OsagoValidator;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Контроллер ОСАГО калькулятора, работает в роли api на REST
 */

@RestController
public class RestApiController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private OsagoCalcServiceImpl osagoCalcService;
    @Autowired
    private OsagoCalcOrderServiceImpl osagoCalcOrderService;
    @Autowired
    private MailClient mailClient;
    @Autowired
    private OsagoCalculator osagoCalculator;
    @Autowired
    private OsagoValidator osagoFormValidator;

    //меняет стандартный шаблон json-парсинга приходящих данных типа Date на шаблон "дд.мм.гггг"
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    //возвращает список городов
    @RequestMapping(method= RequestMethod.GET, value="/api/calculator/osago/1.0/public/getcities")
    public Object calcAPIContainerCity (){
        return osagoCalculator.getCities();
    }

    //возвращает результат калькуляции, соотвествующий параметрам, прилагаемым в теле запроса
    @RequestMapping(method= RequestMethod.GET, value="/api/calculator/osago/1.0/public/calculate")
    public Object calcAPIResultCalculation(
            @RequestParam(value="k1") String k1, @RequestParam(value="k2") String k2, @RequestParam(value="k3a") String k3a,
            @RequestParam(value="k3b") String k3b, @RequestParam(value="k4") String k4, @RequestParam(value="k5") String k5,
            @RequestParam(value="k6") String k6,  @RequestParam(value="k7") String k7, @RequestParam(value="kPark") String kPark,
            @RequestParam(value="kBM") String kBM, @RequestParam(value="kPrivileges") String kPrivileges,
            @RequestParam(value="kFranshiza") String kFranshiza, @RequestParam(value="kOTK") String kOTK) {
        return osagoCalculator.calculate(k1,k2,k3a,k3b,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK);
    }

    //принимает данные форм и добавляет в базу новую заявку на ОСАГО
    @RequestMapping(method= RequestMethod.POST, value="/api/calculator/osago/1.0/public/neworder")
    public ResponseEntity<String> createNewOsagoOrder(
            @RequestParam(value="k1") String k1, @RequestParam(value="k2") String k2, @RequestParam(value="k3b") String k3b,
            @RequestParam(value="k4") String k4, @RequestParam(value="k5") String k5, @RequestParam(value="k6") String k6,
            @RequestParam(value="k7") String k7, @RequestParam(value="kPark") String kPark, @RequestParam(value="kBM") String kBM,
            @RequestParam(value="kPrivileges") String kPrivileges, @RequestParam(value="kFranshiza") String kFranshiza,
            @RequestParam(value="k3a") String k3a,@RequestParam(value="kOTK") String kOTK, @RequestParam(value="kOTKDate") Date kOTKDate,
            @RequestParam(value="k3") String k3,

            @RequestParam(value="inputSecondName") String inputSecondName,
            @RequestParam(value="inputFirstName") String inputFirstName,
            @RequestParam(value="inputPatronymic") String inputPatronymic,
            @RequestParam(value="inputTelephone") String inputTelephone,
            @RequestParam(value="inputEmail") String inputEmail
    ){
        //проводит калькуляцию входных данных, если калькуляция приводит к результату "0.00" - отдаёт http статус 400
        Map<String,Map<String,String>> calculation = osagoCalculator.calculate(k1,k2,k3a,k3b,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK);
        if (calculation.get("result").get("result").equals("0,00")
                || !osagoFormValidator.getFIOValidation(inputFirstName) || !osagoFormValidator.getFIOValidation(inputSecondName)
                || !osagoFormValidator.getFIOValidation(inputPatronymic) || !osagoFormValidator.getTelephoneValidation(inputTelephone)
                || !osagoFormValidator.getMailValidation(inputEmail)){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
        else {
            try {
                //заносит в базу новую заявку на ОСАГО
                OsagoCalcOrder osagoOrder = osagoCalcOrderService.editCoefficients(new OsagoCalcOrder(
                        osagoCalcService.getByCoefficientId(k1).getCoefficientValue(),osagoCalcService.getByCoefficientId(k2).getCoefficientValue(),
                        osagoCalcService.getByCoefficientId(k3).getCoefficientValue(),osagoCalcService.getByCoefficientId(k4).getCoefficientValue(),
                        osagoCalcService.getByCoefficientId(k5).getCoefficientValue(),osagoCalcService.getByCoefficientId(k6).getCoefficientValue(),
                        osagoCalcService.getByCoefficientId(k7).getCoefficientValue(),osagoCalcService.getByCoefficientId(kPark).getCoefficientValue(),
                        osagoCalcService.getByCoefficientId(kBM).getCoefficientValue(),osagoCalcService.getByCoefficientId(kPrivileges).getCoefficientValue(),
                        osagoCalcService.getByCoefficientId(kFranshiza).getCoefficientValue(),osagoCalcService.getByCoefficientId(kOTK).getCoefficientValue(),
                        Double.parseDouble(calculation.get("result").get("result").replace(",",".")),
                        k1,k2,k3,k3a,k3b,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,kOTKDate,
                        inputSecondName,inputFirstName,inputPatronymic,inputTelephone,inputEmail,false, OsagoOrderStatus.NEW.toString(), new Timestamp(System.currentTimeMillis())
                ));

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String formatedkOTKDate = formatter.format(kOTKDate);

                //Отсылает письмо с описанием новой заявки на ОСАГО всем пользователям с правами ROLE_MANAGER
                List<User>mailRecipients = userRepository.findByRoles(roleRepository.findByName("ROLE_MANAGER"));
                for (User recipient : mailRecipients) {
                    if (recipient.isEnabled()){
                        mailClient.prepareAndSendOsagoOrder(
                                recipient.getEmail(), inputSecondName+" "+inputFirstName+" "+inputPatronymic,
                                osagoOrder.getId().toString(),osagoOrder.getK1Value(),osagoCalcService.getByCoefficientId(osagoOrder.getK1Id()).getDescription(),
                                osagoOrder.getK2Value(),osagoCalcService.getByCoefficientId(osagoOrder.getK2Id()).getDescription(),
                                osagoOrder.getK3Value(),osagoCalcService.getByCoefficientId(osagoOrder.getK3aId()).getDescription(),osagoCalcService.getByCoefficientId(osagoOrder.getK3bId()).getDescription(),
                                osagoOrder.getK4Value(),osagoCalcService.getByCoefficientId(osagoOrder.getK4Id()).getDescription(),
                                osagoOrder.getK5Value(),osagoCalcService.getByCoefficientId(osagoOrder.getK5Id()).getDescription(),
                                osagoOrder.getK6Value(),osagoCalcService.getByCoefficientId(osagoOrder.getK6Id()).getDescription(),
                                osagoOrder.getK7Value(),osagoCalcService.getByCoefficientId(osagoOrder.getK7Id()).getDescription(),
                                osagoOrder.getkParkValue(),osagoCalcService.getByCoefficientId(osagoOrder.getkParkId()).getDescription(),
                                osagoOrder.getkBMValue(),osagoCalcService.getByCoefficientId(osagoOrder.getkBMId()).getDescription(),
                                osagoOrder.getkPrivilegesValue(),osagoCalcService.getByCoefficientId(osagoOrder.getkPrivilegesId()).getDescription(),
                                osagoOrder.getkFranshizaValue(),osagoCalcService.getByCoefficientId(osagoOrder.getkFranshizaId()).getDescription(),
                                osagoCalcService.getByCoefficientId(osagoOrder.getNeedOTKId()).getDescription(),formatedkOTKDate,
                                osagoOrder.getResultValue(),osagoOrder.getInputSecondName(),osagoOrder.getInputFirstName(),osagoOrder.getInputPatronymic(),
                                osagoOrder.getInputTelephone(),osagoOrder.getInputEmail(),osagoOrder.getDateAdded().toString()
                        );
                    }
                }
                return ResponseEntity.status(HttpStatus.OK).body("null");
            }
            //в случае ошибок (кроме неверной калькуляции) отдаёт http ответ 500
            catch (Exception ex){
                ex.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("null");
            }
        }
    }

    //Отдаёт список всех заявок (в упрощённом виде) на ОСАГО в виде json объекта
    @RequestMapping(method= RequestMethod.GET, value="/api/calculator/osago/1.0/private/manager/order/getall")
    public Object getListOfAllOsagoOrders(@RequestParam(value="isProcessed", defaultValue="false") boolean isProcessed){
        return osagoCalcOrderService.getSimpleListOfOrders(isProcessed);
    }

    //Принимает id заявки на ОСАГО и отдаёт подробности по ней в виде json объекта.
    @RequestMapping(method= RequestMethod.GET, value="/api/calculator/osago/1.0/private/manager/order/get/{id}")
    public Object getOrderById(@PathVariable(value = "id") long id){
        return osagoCalcOrderService.getById(id);
    }

    //Принимает id заявки на ОСАГО и меняет её поля: status и isProcessed.
    @RequestMapping(method= RequestMethod.POST, value="/api/calculator/osago/1.0/private/manager/order/changestatus/{id}")
    public ResponseEntity<String> setOrderStatus(@PathVariable(value = "id") long id,
                                                 @RequestParam(value = "status") String status){
        try {
            OsagoCalcOrder processedOrder = osagoCalcOrderService.getById(id);

            if (status.equals(OsagoOrderStatus.NEW.toString())){
                processedOrder.setIsProcessed(false);
                processedOrder.setStatus(status);
                osagoCalcOrderService.editCoefficients(processedOrder);
                return ResponseEntity.status(HttpStatus.OK).body("null");
            }
            if (status.equals(OsagoOrderStatus.CLOSED_SUCCESSFULLY.toString())){
                processedOrder.setIsProcessed(true);
                processedOrder.setStatus(status);
                osagoCalcOrderService.editCoefficients(processedOrder);
                return ResponseEntity.status(HttpStatus.OK).body("null");
            }
            if (status.equals(OsagoOrderStatus.CLOSED_UNSUCCESSFULLY.toString())){
                processedOrder.setIsProcessed(true);
                processedOrder.setStatus(status);
                osagoCalcOrderService.editCoefficients(processedOrder);
                return ResponseEntity.status(HttpStatus.OK).body("null");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
        catch (NullPointerException ex){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("null");
        }
    }

    //отдаёт все коэффициенты ОСАГО
    @RequestMapping(method= RequestMethod.GET, value="/api/calculator/osago/1.0/private/admin/coefficient/getall")
    public ResponseEntity getCoefficients(){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(osagoCalcService.getAll());
        }
        catch (Exception ex){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("null");
        }
    }

    //Добавляет или меняет коэффициент ОСАГО
    @RequestMapping(method= RequestMethod.POST, value="/api/calculator/osago/1.0/private/admin/coefficient/edit")
    public ResponseEntity editCoefficient(@RequestParam(value = "coefficientId") String coefficientId,
                                          @RequestParam(value = "coefficientGroup") String coefficientGroup,
                                          @RequestParam(value = "coefficientValue") double coefficientValue,
                                          @RequestParam(value = "description") String description,
                                          @RequestParam(value = "sortingVal") int sortingVal){
        try {
            osagoCalcService.editCoefficients(new OsagoCoefficients(coefficientId,coefficientGroup,coefficientValue,description,sortingVal));
            return ResponseEntity.status(HttpStatus.OK).body("null");
        }
        catch (Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }

    //удаляет коэффициент ОСАГО
    @RequestMapping(method= RequestMethod.POST, value="/api/calculator/osago/1.0/private/admin/coefficient/delete")
    public ResponseEntity deleteCoefficient(@RequestParam(value = "coefficientId") String coefficientId){
        try {
            if (osagoCalcService.deleteCoefficient(coefficientId)){
                return ResponseEntity.status(HttpStatus.OK).body("null");
            }
            else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
            }
        }
        catch (Exception ex){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("null");
        }
    }
}
