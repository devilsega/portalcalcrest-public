package ua.usstandart.osago.validator;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Валидация заполняемых форм ОСАГО
 */
@Component
public class OsagoValidator {

    public boolean getFIOValidation(String name){
        Pattern pFIO = Pattern.compile("^[a-zA-ZА-Яа-яёЁіІїЇ\\-\\'\\s]{1,50}$");
        Matcher matcher = pFIO.matcher(name);
        return (matcher.matches());
    }

    public boolean getMailValidation(String mail){
        Pattern pEmail = Pattern.compile(".+@.+\\..+");
        Matcher matcher = pEmail.matcher(mail);
        return (matcher.matches());
    }

    public boolean getTelephoneValidation(String telephone){
        Pattern pTelephone = Pattern.compile("^[\\+\\(\\)\\d\\-\\'\\s]{1,50}$");
        Matcher matcher = pTelephone.matcher(telephone);
        return (matcher.matches());
    }
}
