package ua.usstandart.osago.service.interfaces;

import ua.usstandart.osago.persistence.model.OsagoCalcOrder;
import java.util.List;

public interface OsagoCalcOrderService {
    List<OsagoCalcOrder>getAll();
    OsagoCalcOrder editCoefficients(OsagoCalcOrder osagoCalcOrder);
    OsagoCalcOrder getById(Long id);
    List<OsagoCalcOrder> getByIsProcessed(boolean isProcessed);
}
