package ua.usstandart.osago.service.interfaces;
import ua.usstandart.osago.persistence.model.OsagoCoefficients;

import java.util.List;

public interface OsagoCalcService{
    OsagoCoefficients getByCoefficientId(String coefficientId);
    List<OsagoCoefficients> getByCoefficientGroup(String coefficientGroup);
    List<OsagoCoefficients>getAll();
    OsagoCoefficients editCoefficients(OsagoCoefficients osagoCoefficients);
}