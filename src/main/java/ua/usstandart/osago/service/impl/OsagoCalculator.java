package ua.usstandart.osago.service.impl;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * калькулятор. Получает входящие данные, считает, отдаёт результат.
 * Метод calculate отдаёт коллекцию с набором валидных полей для пользовательских форм и результатом вычисления.
 * Метод Getcities отдаёт массив городов.
 */

@Service
public class OsagoCalculator {
    private OsagoCalcServiceImpl osagoCalcService;

    //полученные клиентские значения коэфициентов
    private String k1ReceivedValue, k2ReceivedValue, k3aReceivedValue, k3bReceivedValue, k4ReceivedValue, k5ReceivedValue, k6ReceivedValue,
            k7ReceivedValue, kParkReceivedValue, kBMReceivedValue, kPrivilegesReceivedValue, kFranshizaReceivedValue, kOTKReceivedValue;

    private double k1,k2,k3,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,kResult;  //значения коэфициентов для подсчёта результатов

    public OsagoCalculator(OsagoCalcServiceImpl osagoCalcService){
        this.osagoCalcService = osagoCalcService;
    }

    //сеттеры и геттеры
    //double k1,k2,k3,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,kResult;
    private void setK1(Double k1){
        this.k1 = k1;
    }
    private void setK2(Double k2){
        this.k2 = k2;
    }
    private void setK3(Double k3){
        this.k3 = k3;
    }
    private void setK4(Double k4){
        this.k4 = k4;
    }
    private void setK5(Double k5){
        this.k5 = k5;
    }
    private void setK6(Double k6){
        this.k6 = k6;
    }
    private void setK7(Double k7){
        this.k7 = k7;
    }
    private void setKPark(Double kPark){
        this.kPark = kPark;
    }
    private void setkBM(Double kBM){
        this.kBM = kBM;
    }
    private void setKPrivileges(Double kPrivileges){
        this.kPrivileges = kPrivileges;
    }
    private void setKFranshiza(Double kFranshiza){
        this.kFranshiza = kFranshiza;
    }
    private void setKOTK(Double kOTK){
        this.kOTK = kOTK;
    }
    private void setKResult(Double kResult){
        this.kResult = kResult;
    }
    private double getK1(){
        return k1;
    }
    private double getK2(){
        return k2;
    }
    private double getK3(){
        return k3;
    }
    private double getK4(){
        return k4;
    }
    private double getK5(){
        return k5;
    }
    private double getK6(){
        return k6;
    }
    private double getK7(){
        return k7;
    }
    private double getKPark(){
        return kPark;
    }
    private double getKBM(){
        return kBM;
    }
    private double getKPrivileges(){
        return kPrivileges;
    }
    private double getKFranshiza(){
        return kFranshiza;
    }
    private double getKOTK(){
        return kOTK;
    }
    private double getKResult(){
        return kResult;
    }

    //метод валидации полученного значения.
    private double validateCoefficient(String receivedCoefficientId,Map<String,String> validCollection){
        if(validCollection.containsKey(receivedCoefficientId)){
            return osagoCalcService.getByCoefficientId(receivedCoefficientId).getCoefficientValue();
        }
        else{
            return 0.0;
        }
    }

    private double getValueById(String id){
        try{
            return osagoCalcService.getByCoefficientId(id).getCoefficientValue();
        }
        catch (NullPointerException ex){
            return 0.0;
        }
    }

    //метод подсчёта результата
    private Map<String,String> getResult(){
        double kBase = getValueById("basepayment");
        try {
            setKResult(new BigDecimal((kBase*getK1()*getK2()*getK3()*getK4()*getK5()*getK6()*getK7()*getKPark()*getKBM()*getKFranshiza()*getKOTK())/getKPrivileges()).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
            Map<String,String>resultColl = new LinkedHashMap<>();
            resultColl.put("result",String.format( "%.2f",getKResult()));
            return resultColl;
        }
        catch (NumberFormatException ex){
            setKResult(0.0);
            Map<String,String>resultColl = new LinkedHashMap<>();
            resultColl.put("result","0,00");
            return resultColl;
        }
    }

    //основная калькуляция!
    public Map<String,Map<String,String>> calculate(String k1Value,String k2Value,String k3aValue, String k3bValue, String k4Value,String k5Value,String k6Value,
                            String k7Value,String kParkValue,String kBMValue,String kPrivilegesValue,String kFranshizaValue,String kOTKValue){
        this.k1ReceivedValue=k1Value;
        this.k2ReceivedValue =k2Value;
        this.k3aReceivedValue =k3aValue;
        this.k3bReceivedValue =k3bValue;
        this.k4ReceivedValue =k4Value;
        this.k5ReceivedValue =k5Value;
        this.k6ReceivedValue =k6Value;
        this.k7ReceivedValue =k7Value;
        this.kParkReceivedValue =kParkValue;
        this.kBMReceivedValue =kBMValue;
        this.kPrivilegesReceivedValue =kPrivilegesValue;
        this.kFranshizaReceivedValue =kFranshizaValue;
        this.kOTKReceivedValue =kOTKValue;

        Map<String,Map<String,String>>resultCollection = new LinkedHashMap<>();
        resultCollection.put("k1",calculateK1());
        resultCollection.put("k2",calculateK2());
        resultCollection.put("k3a",calculateK3a());
        resultCollection.put("k3b",calculateK3b());
        resultCollection.put("k3",calculateK3());
        resultCollection.put("k4",calculateK4());
        resultCollection.put("k5",calculateK5());
        resultCollection.put("k6",calculateK6());
        resultCollection.put("k7",calculateK7());
        resultCollection.put("kPark",calculateKPark());
        resultCollection.put("kBM",calculateKBM());
        resultCollection.put("kPrivileges",calculateKPrivileges());
        resultCollection.put("kFranshiza",calculateKFranshiza());
        resultCollection.put("kOTK",calculateKOTK());
        resultCollection.put("result",getResult());
        return resultCollection;
    }

    private Map<String,String> calculateK1(){
        Map<String,String> result = new LinkedHashMap<>();
        if(k1ReceivedValue.equals("")){
            result=osagoCalcService.fillCollectionByGroup("k1");
            setK1(0.0);
            return result;
        }

        result=osagoCalcService.fillCollectionByGroup("k1");
        setK1(validateCoefficient(k1ReceivedValue,result));
        return result;
    }

    // города отдаются отдельным методом, этот возвращает null, но расчитывает коэфициент.
    private Map<String,String> calculateK2(){
        Map<String,String>result = new LinkedHashMap<>();
        setK2(getValueById(k2ReceivedValue));
        return result;
    }

    private Map<String,String> calculateK3(){
        Map<String,String>result = new LinkedHashMap<>();
        if (k3bReceivedValue.equals("k3Individual")){
            if(k3aReceivedValue.equals("k3TaxiNo")){
                // result k3IndividualCarMotoNotTaxi
                setK3(getValueById("k3IndividualCarMotoNotTaxi"));
                result=osagoCalcService.fillCollectionById("k3IndividualCarMotoNotTaxi");
                return result;
            }
            else if(k3aReceivedValue.equals("k3TaxiYes")){
                // result k3IndividualCarMotoTaxi
                setK3(getValueById("k3IndividualCarMotoTaxi"));
                result=osagoCalcService.fillCollectionById("k3IndividualCarMotoTaxi");
                return result;
            }
        }
        else if (k3bReceivedValue.equals("k3Corporate")){
            if(k3aReceivedValue.equals("k3TaxiNo")){
                if(k1ReceivedValue.equals("k1b1")| k1ReceivedValue.equals("k1b2")| k1ReceivedValue.equals("k1b3")| k1ReceivedValue.equals("k1b4")
                        | k1ReceivedValue.equals("k1a1")| k1ReceivedValue.equals("k1a2")){
                    // result k3CorporateCarMotoNotTaxi
                    setK3(getValueById("k3CorporateCarMotoNotTaxi"));
                    result=osagoCalcService.fillCollectionById("k3CorporateCarMotoNotTaxi");
                    return result;
                }
                else if(k1ReceivedValue.equals("k1c1")| k1ReceivedValue.equals("k1c2")
                        | k1ReceivedValue.equals("k1d1")| k1ReceivedValue.equals("k1d2")
                        | k1ReceivedValue.equals("k1f1")
                        | k1ReceivedValue.equals("k1e1")){
                    // result k3CorporateTruckBusNotTaxi
                    setK3(getValueById("k3CorporateTruckBusNotTaxi"));
                    result=osagoCalcService.fillCollectionById("k3CorporateTruckBusNotTaxi");
                    return result;
                }
            }
            else if(k3aReceivedValue.equals("k3TaxiYes")){
                // result k3CorporateCarMotoTaxi
                setK3(getValueById("k3CorporateCarMotoTaxi"));
                result=osagoCalcService.fillCollectionById("k3CorporateCarMotoTaxi");
                return result;
            }
        }
        setK3(0.0);
        result=osagoCalcService.fillCollectionByGroup("k3");
        return result;
    }

    private Map<String,String> calculateK3a(){
        Map<String,String>result = new LinkedHashMap<>();

        if((k1ReceivedValue.equals("k1c1")| k1ReceivedValue.equals("k1c2")| k1ReceivedValue.equals("k1d2")
                | k1ReceivedValue.equals("k1f1")| k1ReceivedValue.equals("k1e1")
                | k1ReceivedValue.equals("k1a1")| k1ReceivedValue.equals("k1a2"))
                | osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone7")){
            result=osagoCalcService.fillCollectionById("k3TaxiNo");
            return result;
        }

        else if (k1ReceivedValue.equals("k1b1")| k1ReceivedValue.equals("k1b2")| k1ReceivedValue.equals("k1b3")| k1ReceivedValue.equals("k1b4")
                | k1ReceivedValue.equals("k1d1")){
            result=osagoCalcService.fillCollectionByGroup("k3a");
            return result;
        }

        result=osagoCalcService.fillCollectionByGroup("k3a");
        setK3(0.0);
        return result;
    }

    private Map<String,String> calculateK3b(){
        Map<String,String>result = new LinkedHashMap<>();
        result=osagoCalcService.fillCollectionByGroup("k3b");
        return result;
    }

    private Map<String,String> calculateK4(){
        Map<String,String>result = new LinkedHashMap<>();

        if (kFranshizaReceivedValue.equals("kFranshiza0uah")){
            if (k3bReceivedValue.equals("k3Individual")){
                // result franshiza0RegardlessOfDrivingExp
                setK4(getValueById("k4Franshiza0RegardlessOfDrivingExp"));
                result = osagoCalcService.fillCollectionById("k4Franshiza0RegardlessOfDrivingExp");
                return result;
            }
            else if (k3bReceivedValue.equals("k3Corporate")){
                // result franshiza0Corporate
                setK4(getValueById("k4Franshiza0Corporate"));
                result=osagoCalcService.fillCollectionById("k4Franshiza0Corporate");
                return result;
            }
        }
        else if (kFranshizaReceivedValue.equals("kFranshiza2000uah")| kFranshizaReceivedValue.equals("kFranshiza1000uah")
                | kFranshizaReceivedValue.equals("kFranshiza500uah")){
            if (k3bReceivedValue.equals("k3Individual")){
                // result franshizaNot0RegardlessOfDrivingExp
                setK4(getValueById("k4FranshizaNot0RegardlessOfDrivingExp"));
                result=osagoCalcService.fillCollectionById("k4FranshizaNot0RegardlessOfDrivingExp");
                return result;
            }
            else if (k3bReceivedValue.equals("k3Corporate")){
                // result franshizaNot0Corporate
                setK4(getValueById("k4FranshizaNot0Corporate"));
                result = osagoCalcService.fillCollectionById("k4FranshizaNot0Corporate");
                return result;
            }
        }
        result=osagoCalcService.fillCollectionByGroup("k4");
        setK4(0.0);
        return result;
    }

    private Map<String,String> calculateK5(){
        Map<String,String>result = new LinkedHashMap<>();
        if(k5ReceivedValue.equals("")){
            result = osagoCalcService.fillCollectionByGroup("k5");
            setK5(0.0);
            return result;
        }

        result=osagoCalcService.fillCollectionByGroup("k5");
        setK5(validateCoefficient(k5ReceivedValue,result));
        return result;
    }

    private Map<String,String> calculateK6(){
        Map<String,String>result = new LinkedHashMap<>();
        if(k6ReceivedValue.equals("")){
            result = osagoCalcService.fillCollectionByGroup("k6");
            setK6(0.0);
            return result;
        }

        result = osagoCalcService.fillCollectionByGroup("k6");
        setK6(validateCoefficient(k6ReceivedValue,result));
        return result;
    }

    private Map<String,String> calculateK7(){
        Map<String,String>result = new LinkedHashMap<>();

        if (osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone7")){
            result = osagoCalcService.fillCollectionByGroup("k7");
            setK7(validateCoefficient(k7ReceivedValue,result));
            return result;
        }
        else if (osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone1")| osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone2")
                | osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone3")| osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone4")
                | osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone5")| osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone6")){
            result=osagoCalcService.fillCollectionById("k7OneYear");
            setK7(validateCoefficient(k7ReceivedValue,result));
            return result;
        }
        result=osagoCalcService.fillCollectionByGroup("k7");
        setK7(0.0);
        return result;
    }

    private Map<String,String> calculateKPark(){
        Map<String,String>result = new LinkedHashMap<>();
        if(kParkReceivedValue.equals("")){
            result=osagoCalcService.fillCollectionByGroup("kPark");
            setKPark(0.0);
            return result;
        }

        result=osagoCalcService.fillCollectionByGroup("kPark");
        setKPark(validateCoefficient(kParkReceivedValue,result));
        return result;
    }

    private Map<String,String> calculateKBM(){
        Map<String,String>result = new LinkedHashMap<>();
        if(kBMReceivedValue.equals("")){
            result=osagoCalcService.fillCollectionByGroup("kBM");
            setkBM(0.0);
            return result;
        }
        switch (kPrivilegesReceivedValue) {
            case "kPrivilegesYes":{
                result.putAll(osagoCalcService.fillCollectionById("kBMM"));
                result.putAll(osagoCalcService.fillCollectionById("kBM0"));
                result.putAll(osagoCalcService.fillCollectionById("kBM1"));
                result.putAll(osagoCalcService.fillCollectionById("kBM2"));
                result.putAll(osagoCalcService.fillCollectionById("kBM3"));
                setkBM(validateCoefficient(kBMReceivedValue,result));
                return result;
            }
            case "kPrivilegesNo":
            default:{
                result=osagoCalcService.fillCollectionByGroup("kBM");
                setkBM(validateCoefficient(kBMReceivedValue,result));
                return result;
            }
        }
    }

    private Map<String,String> calculateKPrivileges(){
        Map<String,String>result = new LinkedHashMap<>();

        if(k3bReceivedValue.equals("k3Corporate")| k3aReceivedValue.equals("k3TaxiYes")){
            result=osagoCalcService.fillCollectionById("kPrivilegesNo");
            setKPrivileges(validateCoefficient(kPrivilegesReceivedValue,result));
            return result;
        }
        else if(k3bReceivedValue.equals("k3Individual")| k3aReceivedValue.equals("k3TaxiNo")){
            result=osagoCalcService.fillCollectionByGroup("kPrivileges");
            setKPrivileges(validateCoefficient(kPrivilegesReceivedValue,result));
            return result;
        }
        result=osagoCalcService.fillCollectionByGroup("kPrivileges");
        setKPrivileges(0.0);
        return result;
    }

    private Map<String,String> calculateKFranshiza(){
        Map<String,String>result = new LinkedHashMap<>();

        if(k3bReceivedValue.equals("k3Individual")){
            if(osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone1")| osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone2")
                    | osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone3")){
                if(k3aReceivedValue.equals("k3TaxiYes")){
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza2000uah"));
                    setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                    return result;
                }
                else if(k3aReceivedValue.equals("k3TaxiNo")){
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza1000uah"));
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza0uah"));
                    setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                    return result;
                }
            }
            else if(osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone4")){
                if(k3aReceivedValue.equals("k3TaxiYes")){
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza2000uah"));
                    setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                    return result;
                }
                else if(k3aReceivedValue.equals("k3TaxiNo")){
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza500uah"));
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza0uah"));
                    setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                    return result;
                }
            }
            else if(osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone5")| osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone6")){
                if(k3aReceivedValue.equals("k3TaxiYes")){
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza1000uah"));
                    setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                    return result;
                }
                else if(k3aReceivedValue.equals("k3TaxiNo")){
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza500uah"));
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza0uah"));
                    setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                    return result;
                }
            }
            else if(osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone7")){
                result.putAll(osagoCalcService.fillCollectionById("kFranshiza500uah"));
                result.putAll(osagoCalcService.fillCollectionById("kFranshiza0uah"));
                setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                return result;
            }
        }
        else if(k3bReceivedValue.equals("k3Corporate")){
            if(k3aReceivedValue.equals("k3TaxiYes")){
                if(osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone1")| osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone2")
                        | osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone3")| osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone4")){
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza2000uah"));
                    setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                    return result;
                }
                else if(osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone5")| osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone6")){
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza2000uah"));
                    result.putAll(osagoCalcService.fillCollectionById("kFranshiza1000uah"));
                    setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                    return result;
                }
                else if(osagoCalcService.checkIdBelongsToGroup(k2ReceivedValue,"k2zone7")){
                    result.putAll(osagoCalcService.fillCollectionByGroup("kFranshiza"));
                    setKFranshiza(0.0);
                    return result;
                }
            }
            else if(k3aReceivedValue.equals("k3TaxiNo")){
                result.putAll(osagoCalcService.fillCollectionByGroup("kFranshiza"));
                setKFranshiza(validateCoefficient(kFranshizaReceivedValue,result));
                return result;
            }
        }
        result.putAll(osagoCalcService.fillCollectionByGroup("kFranshiza"));
        setKFranshiza(0.0);
        return result;
    }

    private Map<String,String> calculateKOTK(){
        Map<String,String>result = new LinkedHashMap<>();
        if(kOTKReceivedValue.equals("")){
            result=osagoCalcService.fillCollectionByGroup("kOTK");
            setKOTK(0.0);
            return result;
        }

        result=osagoCalcService.fillCollectionByGroup("kOTK");
        setKOTK(validateCoefficient(kOTKReceivedValue,result));
        return result;
    }

    public String[] getCities(){
        Map<String,String>temp = new LinkedHashMap<>();
        temp.putAll(osagoCalcService.fillCollectionByGroup("k2zone1"));
        temp.putAll(osagoCalcService.fillCollectionByGroup("k2zone2"));
        temp.putAll(osagoCalcService.fillCollectionByGroup("k2zone3"));
        temp.putAll(osagoCalcService.fillCollectionByGroup("k2zone4"));
        temp.putAll(osagoCalcService.fillCollectionByGroup("k2zone5"));
        temp.putAll(osagoCalcService.fillCollectionByGroup("k2zone6"));
        temp.putAll(osagoCalcService.fillCollectionByGroup("k2zone7"));

        String[]result = new String[temp.size()];
        int i = 0;
        for (String key:temp.keySet()) {
            result[i] = key;
            i++;
        }
        return result;
    }
}
