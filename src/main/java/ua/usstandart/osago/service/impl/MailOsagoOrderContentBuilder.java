package ua.usstandart.osago.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Сервис, формирующий html страницу для отправки её по почте
 */

@Service
public class MailOsagoOrderContentBuilder {

    private TemplateEngine templateEngine;

    @Autowired
    public MailOsagoOrderContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String build(String id, double k1Value, String k1Description, double k2Value, String k2Description, double k3Value, String k3aDescription, String k3bDescription,
                        double k4Value, String k4Description, double k5Value, String k5Description, double k6Value, String k6Description,
                        double k7Value, String k7Description, double kParkValue, String kParkDescription, double kBMValue, String kBMDescription,
                        double kPrivilegesValue, String kPrivilegesDescription, double kFranshizaValue, String kFranshizaDescription,
                        String kOTKDescription, String kOTKDate, double kResultValue, String inputSecondName, String inputFirstName, String inputPatronymic,
                        String inputTelephone, String inputEmail, String inputDate) {
        Context context = new Context();
        context.setVariable("id", id);
        context.setVariable("k1Value", k1Value);
        context.setVariable("k1Description", k1Description);
        context.setVariable("k2Value", k2Value);
        context.setVariable("k2Description", k2Description);
        context.setVariable("k3Value", k3Value);
        context.setVariable("k3aDescription", k3aDescription);
        context.setVariable("k3bDescription", k3bDescription);
        context.setVariable("k4Value", k4Value);
        context.setVariable("k4Description", k4Description);
        context.setVariable("k5Value", k5Value);
        context.setVariable("k5Description", k5Description);
        context.setVariable("k6Value", k6Value);
        context.setVariable("k6Description", k6Description);
        context.setVariable("k7Value", k7Value);
        context.setVariable("k7Description", k7Description);
        context.setVariable("kParkValue", kParkValue);
        context.setVariable("kParkDescription", kParkDescription);
        context.setVariable("kBMValue", kBMValue);
        context.setVariable("kBMDescription", kBMDescription);
        context.setVariable("kPrivilegesValue", kPrivilegesValue);
        context.setVariable("kPrivilegesDescription", kPrivilegesDescription);
        context.setVariable("kFranshizaValue", kFranshizaValue);
        context.setVariable("kFranshizaDescription", kFranshizaDescription);
        context.setVariable("kOTKDescription", kOTKDescription);
        context.setVariable("kOTKDate", kOTKDate);
        context.setVariable("kResultValue", kResultValue);
        context.setVariable("inputSecondName", inputSecondName);
        context.setVariable("inputFirstName", inputFirstName);
        context.setVariable("inputPatronymic", inputPatronymic);
        context.setVariable("inputTelephone", inputTelephone);
        context.setVariable("inputEmail", inputEmail);
        context.setVariable("inputDate", inputDate);
        return templateEngine.process("mailTemplate", context);
    }
}
