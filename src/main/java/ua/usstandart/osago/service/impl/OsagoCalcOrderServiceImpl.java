package ua.usstandart.osago.service.impl;

import org.springframework.stereotype.Service;
import ua.usstandart.osago.domainObject.OsagoCalcOrderSimpleForm;
import ua.usstandart.osago.persistence.model.OsagoCalcOrder;
import ua.usstandart.osago.persistence.dao.OsagoCalcOrderRepository;
import ua.usstandart.osago.service.interfaces.OsagoCalcOrderService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Основная логика по добавлению новых и работе с имеющимися заявками на покупку КАСКО
 * ver.1.0
 */

@Service
public class OsagoCalcOrderServiceImpl implements OsagoCalcOrderService{

    private OsagoCalcOrderRepository osagoCalcOrderRepository;

    public OsagoCalcOrderServiceImpl (OsagoCalcOrderRepository osagoCalcOrderRepository){
        this.osagoCalcOrderRepository = osagoCalcOrderRepository;
    }

    @Override
    public List<OsagoCalcOrder> getAll(){
        return osagoCalcOrderRepository.findAll();
    }

    @Override
    public OsagoCalcOrder editCoefficients(OsagoCalcOrder osagoCalcOrder){
        return osagoCalcOrderRepository.saveAndFlush(osagoCalcOrder);
    }

    @Override
    public OsagoCalcOrder getById(Long id){
        return osagoCalcOrderRepository.findOne(id);
    }

    @Override
    public List<OsagoCalcOrder> getByIsProcessed(boolean isProcessed){
        return osagoCalcOrderRepository.findByIsProcessed(isProcessed);
    }

    public List<OsagoCalcOrderSimpleForm> getSimpleListOfOrders(boolean isProcessed){
        List<OsagoCalcOrderSimpleForm>result = new ArrayList<>();
        List<OsagoCalcOrder>items = osagoCalcOrderRepository.findByIsProcessed(isProcessed);
        for (OsagoCalcOrder item:items) {
            result.add(new OsagoCalcOrderSimpleForm(item.getId(),item.getInputFirstName(),item.getInputSecondName(),item.getInputPatronymic(),
            item.getStatus(),item.getIsprocessed(),item.getDateAdded()));
        }

        Collections.sort(result, new Comparator<OsagoCalcOrderSimpleForm>() {
            public int compare(OsagoCalcOrderSimpleForm o1, OsagoCalcOrderSimpleForm o2) {
                return o1.getDateAdded().compareTo(o2.getDateAdded());
            }
        });

        return result;
    }
}
