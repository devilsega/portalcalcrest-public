package ua.usstandart.osago.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

/**
 * Сервис для отправки почтовых сообщений (SMTP сервер настраивается в application.properties)
 */

@Service
public class MailClient {

    private JavaMailSender mailSender;
    private MailOsagoOrderContentBuilder mailOsagoOrderContentBuilder;
    @Value( "${mail.sender}" )
    private String sender;

    @Autowired
    public MailClient(JavaMailSender mailSender, MailOsagoOrderContentBuilder mailOsagoOrderContentBuilder) {
        this.mailSender = mailSender;
        this.mailOsagoOrderContentBuilder = mailOsagoOrderContentBuilder;
    }

    public void prepareAndSendOsagoOrder(String recipient, String subject,
                                         String id, double k1Value, String k1Description, double k2Value, String k2Description, double k3Value, String k3aDescription, String k3bDescription,
                                         double k4Value, String k4Description, double k5Value, String k5Description, double k6Value, String k6Description,
                                         double k7Value, String k7Description, double kParkValue, String kParkDescription, double kBMValue, String kBMDescription,
                                         double kPrivilegesValue, String kPrivilegesDescription, double kFranshizaValue, String kFranshizaDescription,
                                         String kOTKDescription, String kOTKDate, double kResultValue, String inputSecondName, String inputFirstName, String inputPatronymic,
                                         String inputTelephone, String inputEmail, String date) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(sender);
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Новая заявка на ОСАГО "+subject);

            String content = mailOsagoOrderContentBuilder.build(id, k1Value, k1Description, k2Value, k2Description, k3Value, k3aDescription, k3bDescription, k4Value, k4Description,
                    k5Value, k5Description, k6Value, k6Description, k7Value, k7Description, kParkValue, kParkDescription, kBMValue, kBMDescription, kPrivilegesValue,
                    kPrivilegesDescription, kFranshizaValue, kFranshizaDescription, kOTKDescription, kOTKDate, kResultValue, inputSecondName, inputFirstName, inputPatronymic,
                    inputTelephone, inputEmail, date);
            messageHelper.setText(content, true);

        };
        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            // runtime exception; compiler will not force you to handle it //TODO: log error
            e.printStackTrace();
        }
    }
}