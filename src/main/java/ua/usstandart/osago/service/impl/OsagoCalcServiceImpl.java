package ua.usstandart.osago.service.impl;

import org.springframework.stereotype.Service;
import ua.usstandart.osago.persistence.model.OsagoCoefficients;
import ua.usstandart.osago.persistence.dao.OsagoCoefficientsRepository;
import ua.usstandart.osago.service.interfaces.OsagoCalcService;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *  Сервисный слой по работе с дао OsagoCoefficients
 */

@Service
public class OsagoCalcServiceImpl implements OsagoCalcService {
    private OsagoCoefficientsRepository osagoCoefficientsRepository;

    public OsagoCalcServiceImpl(OsagoCoefficientsRepository osagoCoefficientsRepository){
        this.osagoCoefficientsRepository = osagoCoefficientsRepository;
    }

    @Override
    public OsagoCoefficients getByCoefficientId (String coefficientId){
        return osagoCoefficientsRepository.findOne(coefficientId);
    }

    @Override
    public List<OsagoCoefficients> getByCoefficientGroup (String coefficientGroup){
        return osagoCoefficientsRepository.findByCoefficientGroup(coefficientGroup);
    }

    @Override
    public List<OsagoCoefficients> getAll(){
        return osagoCoefficientsRepository.findAll();
    }

    @Override
    public OsagoCoefficients editCoefficients(OsagoCoefficients osagoCoefficients){
        return osagoCoefficientsRepository.saveAndFlush(osagoCoefficients);
    }

    public boolean deleteCoefficient(String id){
        if (osagoCoefficientsRepository.exists(id)){
            osagoCoefficientsRepository.delete(id);
            return true;
        }
        else {
            return false;
        }
    }

    public Map<String,String> fillCollectionByGroup(String coeffficientGroup){
        try {
            List<OsagoCoefficients> temp = getByCoefficientGroup(coeffficientGroup);
            LinkedHashMap<String,String>result = new LinkedHashMap<>();
            for (OsagoCoefficients item:temp) {
                result.put(item.getCoefficientId(),item.getDescription());
            }
            return result;
        } catch (NullPointerException ex){
            return null;
        }
    }

    public Map<String,String> fillCollectionById(String coeffficientId){
        try {
            OsagoCoefficients temp = getByCoefficientId(coeffficientId);
            LinkedHashMap<String,String>result = new LinkedHashMap<>();
            result.put(temp.getCoefficientId(),temp.getDescription());
            return result;
        } catch (NullPointerException ex){
            return null;
        }
    }

    private String getGroupById(String id){
        try {
            return  osagoCoefficientsRepository.findOne(id).getCoefficientGroup();
        }
        catch (NullPointerException ex){
            return null;
        }
    }

    public boolean checkIdBelongsToGroup(String id, String group){
        try {
            if (getGroupById(id).equals(group))return true;
            else return false;
        }
        catch (NullPointerException ex){
            return false;
        }
    }
}
