package ua.usstandart.osago.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * конфигурация непараметризованных шаблонов thymeleaf
 */

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("osagoCalc");
        registry.addViewController("/cabinet").setViewName("cabinet");
        registry.addViewController("/login").setViewName("login");
    }
}
