package ua.usstandart.osago.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ua.usstandart.osago.security.AuthUserDetailsService;

@EnableWebSecurity
public class SecurityConfig{

    @Autowired
    private AuthUserDetailsService authUserDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider
                = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(authUserDetailsService);
        authProvider.setPasswordEncoder(bCryptPasswordEncoder());
        return authProvider;
    }

    @Configuration
    @Order(1)
    public static class RestApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Autowired
        SecurityConfig securityConfig;

        @Override
        protected void configure(AuthenticationManagerBuilder auth)
                throws Exception {
            auth.authenticationProvider(securityConfig.authenticationProvider());
        }

        protected void configure(HttpSecurity http) throws Exception {
            http
            .antMatcher("/api/calculator/osago/1.0/public/**").sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
                    .and().csrf().disable();
        }
    }

    @Configuration
    @Order(2)
    public static class RestApiWebSecurityConfigurationAdapter1 extends WebSecurityConfigurerAdapter {
        @Autowired
        SecurityConfig securityConfig;

        @Override
        protected void configure(AuthenticationManagerBuilder auth)
                throws Exception {
            auth.authenticationProvider(securityConfig.authenticationProvider());
        }

        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/api/calculator/osago/1.0/private/manager/**").sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
                    .and()
                    .authorizeRequests().anyRequest().hasAnyRole("ADMIN","MANAGER")
                    .and()
                    .httpBasic()
                    .and()
                    .csrf().disable();
        }
    }

    @Configuration
    @Order(3)
    public static class RestApiWebSecurityConfigurationAdapter2 extends WebSecurityConfigurerAdapter {
        @Autowired
        SecurityConfig securityConfig;

        @Override
        protected void configure(AuthenticationManagerBuilder auth)
                throws Exception {
            auth.authenticationProvider(securityConfig.authenticationProvider());
        }

        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/api/calculator/osago/1.0/private/admin/**").sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
                    .and()
                    .authorizeRequests().anyRequest().hasRole("ADMIN")
                    .and()
                    .httpBasic()
                    .and()
                    .csrf().disable();
        }
    }

    @Configuration
    @Order(4)
    public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
        @Autowired
        SecurityConfig securityConfig;

        @Override
        protected void configure(AuthenticationManagerBuilder auth)
                throws Exception {
            auth.authenticationProvider(securityConfig.authenticationProvider());
        }


        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .authorizeRequests()
                    .antMatchers("/", "/login", "/error").permitAll()
                    .antMatchers("/cabinet").hasAnyRole("ADMIN","MANAGER")
                    .antMatchers("/cabinet/coefficients-osago/**").hasRole("ADMIN")
                    .antMatchers("/cabinet/orders-osago/**").hasAnyRole("ADMIN","MANAGER")
                    .antMatchers("/api/calculator/osago/1.0/private/manager/**").hasAnyRole("ADMIN","MANAGER")
                    .antMatchers("/api/calculator/osago/1.0/private/admin/**").hasRole("ADMIN")
                    .and()
                    .formLogin()
                    .loginPage("/login").defaultSuccessUrl("/cabinet")
                    .and()
                    .logout().logoutSuccessUrl("/login?logout")
                    .deleteCookies("JSESSIONID")
                    .invalidateHttpSession(true);
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web
                    .ignoring()
                    .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**");
        }
    }
}
