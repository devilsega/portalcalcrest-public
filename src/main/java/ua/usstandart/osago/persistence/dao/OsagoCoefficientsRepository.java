package ua.usstandart.osago.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ua.usstandart.osago.persistence.model.OsagoCoefficients;

import java.util.List;

/**
 * OsagoCoefficients Repo
 */

public interface OsagoCoefficientsRepository extends JpaRepository<OsagoCoefficients,String> {

    @Query(value = "SELECT o.coefficient_id, o.coefficient_group, o.coefficient_value, o.description, o.sorting_val FROM dbo.osago_coefficients o WHERE o.coefficient_group= :coefficientGroup ORDER BY o.sorting_val", nativeQuery = true)
    List<OsagoCoefficients> findByCoefficientGroup(@Param("coefficientGroup") String coefficientGroup);

    @Query(value = "SELECT * FROM dbo.osago_coefficients ORDER BY coefficient_group, sorting_val", nativeQuery = true)
    List<OsagoCoefficients> findAll();
}