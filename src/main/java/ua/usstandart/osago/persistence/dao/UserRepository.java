package ua.usstandart.osago.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.usstandart.osago.persistence.model.Role;
import ua.usstandart.osago.persistence.model.User;

import java.util.List;


public interface UserRepository extends JpaRepository<User, String> {
    User findByEmail(String email);
    List<User> findByRoles(Role role);
}
