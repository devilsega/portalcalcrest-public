package ua.usstandart.osago.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.usstandart.osago.persistence.model.Role;

public interface RoleRepository extends JpaRepository<Role,Long>{
    Role findById(Long id);
    Role findByName(String name);
}
