package ua.usstandart.osago.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.usstandart.osago.persistence.model.OsagoCalcOrder;

import java.util.List;

/**
 * репозитарий для OsagoCalcOrder
 */

public interface OsagoCalcOrderRepository extends JpaRepository<OsagoCalcOrder,Long> {
    List<OsagoCalcOrder> findById(long id);
    List<OsagoCalcOrder> findByIsProcessed(boolean isProcessed);
}
