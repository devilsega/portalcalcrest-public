package ua.usstandart.osago.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.usstandart.osago.persistence.model.Privilege;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

    Privilege findByName(String name);
}