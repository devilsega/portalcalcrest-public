package ua.usstandart.osago.persistence.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Объект, отвечающий за коэфициенты калькулятора ОСАГО.
 */

@Entity
@Table(name = "osago_coefficients", schema="dbo")
public class OsagoCoefficients {

    @Id
    @NotNull
    @Size(min = 1,max=50)
    @Column(name = "coefficient_id", unique=true)
    private String coefficientId;

    @NotNull
    @Size(min = 1,max=50)
    @Column(name = "coefficient_group")
    private String coefficientGroup;

    @NotNull
    @Column(name = "coefficient_value")
    private double coefficientValue;

    @NotNull
    @Size(min = 1,max=500)
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "sorting_val")
    private int sortingVal;

    public OsagoCoefficients(){}

    public OsagoCoefficients(String coefficientId, String coefficientGroup, Double coefficientValue, String description, int sortingVal){
        this.coefficientId = coefficientId;
        this.coefficientGroup = coefficientGroup;
        this.coefficientValue = coefficientValue;
        this.description = description;
        this.sortingVal = sortingVal;

    }

    //getters and setters

    public String getCoefficientId(){
        return coefficientId;
    }

    public String getCoefficientGroup(){
        return coefficientGroup;
    }

    public double getCoefficientValue(){
        return coefficientValue;
    }

    public String getDescription(){
        return description;
    }

    public int getSortingVal(){
        return sortingVal;
    }

    public void setCoefficientId(String coefficientId){
        this.coefficientId = coefficientId;
    }

    public void setCoefficient(String coefficientGroup){
        this.coefficientGroup = coefficientGroup;
    }

    public void setCoefficientValue(double coefficientValue){
        this.coefficientValue = coefficientValue;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setSortingVal(int sortingVal){
        this.sortingVal = sortingVal;
    }
}
