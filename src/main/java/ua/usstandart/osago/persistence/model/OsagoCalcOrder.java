package ua.usstandart.osago.persistence.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Заявка на ОСАГО
 */

@Entity
@Table(name = "osago_order", schema="dbo")
public class OsagoCalcOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "k1value")
    private double k1Value;

    @NotNull
    @Column(name = "k2value")
    private double k2Value;

    @NotNull
    @Column(name = "k3value")
    private double k3Value;

    @NotNull
    @Column(name = "k4value")
    private double k4Value;

    @NotNull
    @Column(name = "k5value")
    private double k5Value;

    @NotNull
    @Column(name = "k6value")
    private double k6Value;

    @NotNull
    @Column(name = "k7value")
    private double k7Value;

    @NotNull
    @Column(name = "kparkvalue")
    private double kParkValue;

    @NotNull
    @Column(name = "kbmvalue")
    private double kBMValue;

    @NotNull
    @Column(name = "kprivilegesvalue")
    private double kPrivilegesValue;

    @NotNull
    @Column(name = "kfranshizavalue")
    private double kFranshizaValue;

    @NotNull
    @Column(name = "needotkvalue")
    private double needOTKValue;

    @NotNull
    @Column(name = "calc_resultvalue")
    private double resultValue;

    @NotNull
    @Column(name = "k1id")
    private String k1Id;

    @NotNull
    @Column(name = "k2id")
    private String k2Id;

    @NotNull
    @Column(name = "k3id")
    private String k3Id;

    @NotNull
    @Column(name = "k3aid")
    private String k3aId;

    @NotNull
    @Column(name = "k3bid")
    private String k3bId;

    @NotNull
    @Column(name = "k4id")
    private String k4Id;

    @NotNull
    @Column(name = "k5id")
    private String k5Id;

    @NotNull
    @Column(name = "k6id")
    private String k6Id;

    @NotNull
    @Column(name = "k7id")
    private String k7Id;

    @NotNull
    @Column(name = "kparkid")
    private String kParkId;

    @NotNull
    @Column(name = "kbmid")
    private String kBMId;

    @NotNull
    @Column(name = "kprivilegesid")
    private String kPrivilegesId;

    @NotNull
    @Column(name = "kfranshizaid")
    private String kFranshizaId;

    @NotNull
    @Column(name = "needotkid")
    private String needOTKId;

    @NotNull
    @Type(type="date")
    @Column(name = "kotkdate")
    private Date kOTKDate;

    @Column(name = "inputsecondname")
    @Size(min=1,max=50)
    @NotNull
    private String inputSecondName;

    @Column(name = "inputfirstname")
    @Size(min=1,max=50)
    @NotNull
    private String inputFirstName;

    @Column(name = "inputpatronymic")
    @Size(min=1,max=50)
    @NotNull
    private String inputPatronymic;

    @NotNull
    @Size(min=1,max=50)
    @Column(name = "inputtelephone")
    private String inputTelephone;

    @Column(name = "inputemail")
    @Size(min=1,max=100)
    @NotNull
    private String inputEmail;

    @Column(name = "isprocessed")
    @NotNull
    private Boolean isProcessed;

    @Column(name = "status")
    @Size(min=1,max=100)
    @NotNull
    private String status;

    @NotNull
    @Column(name = "date_added")
    private Timestamp dateAdded;

    private OsagoCalcOrder() {}

    public OsagoCalcOrder(double k1Value,double k2Value,double k3Value,double k4Value,double k5Value,double k6Value,double k7Value,
                          double kParkValue,double kBMValue, double kPrivilegesValue,double kFranshizaValue, double needOTKValue, double resultValue,
                          String k1Id,String k2Id,String k3Id, String k3aId,String k3bId, String k4Id,String k5Id,String k6Id, String k7Id,
                          String kParkId,String kBMId, String kPrivilegesId,String kFranshizaId, String needOTKId, Date kOTKDate,
                          String inputSecondName, String inputFirstName, String inputPatronymic, String inputTelephone,
                          String inputEmail, Boolean isProcessed, String status, Timestamp dateAdded){
        this.k1Value = k1Value;
        this.k2Value = k2Value;
        this.k3Value = k3Value;
        this.k4Value = k4Value;
        this.k5Value = k5Value;
        this.k6Value = k6Value;
        this.k7Value = k7Value;
        this.kParkValue = kParkValue;
        this.kBMValue = kBMValue;
        this.kPrivilegesValue = kPrivilegesValue;
        this.kFranshizaValue = kFranshizaValue;
        this.needOTKValue = needOTKValue;
        this.resultValue = resultValue;
        this.k1Id = k1Id;
        this.k2Id = k2Id;
        this.k3Id = k3Id;
        this.k3aId = k3aId;
        this.k3bId = k3bId;
        this.k4Id = k4Id;
        this.k5Id = k5Id;
        this.k6Id = k6Id;
        this.k7Id = k7Id;
        this.kParkId = kParkId;
        this.kBMId = kBMId;
        this.kPrivilegesId = kPrivilegesId;
        this.kFranshizaId = kFranshizaId;
        this.needOTKId = needOTKId;
        this.kOTKDate = kOTKDate;
        this.inputSecondName = inputSecondName;
        this.inputFirstName = inputFirstName;
        this.inputPatronymic = inputPatronymic;
        this.inputTelephone = inputTelephone;
        this.inputEmail = inputEmail;
        this.isProcessed = isProcessed;
        this.status = status;
        this.dateAdded = dateAdded;
    }

    public void setIsProcessed(Boolean isProcessed){
        this.isProcessed = isProcessed;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public double getK1Value() {
        return k1Value;
    }

    public double getK2Value() {
        return k2Value;
    }

    public double getK3Value() {
        return k3Value;
    }

    public double getK4Value() {
        return k4Value;
    }

    public double getK5Value() {
        return k5Value;
    }

    public double getK6Value() {
        return k6Value;
    }

    public double getK7Value() {
        return k7Value;
    }

    public double getkParkValue() {
        return kParkValue;
    }

    public double getkBMValue() {
        return kBMValue;
    }

    public double getkPrivilegesValue() {
        return kPrivilegesValue;
    }

    public double getkFranshizaValue() {
        return kFranshizaValue;
    }

    public double getNeedOTKValue() {
        return needOTKValue;
    }

    public double getResultValue() {
        return resultValue;
    }

    public String getK1Id() {
        return k1Id;
    }

    public String getK2Id() {
        return k2Id;
    }

    public String getK3Id() {
        return k3Id;
    }

    public String getK3aId() {
        return k3aId;
    }

    public String getK3bId() {
        return k3bId;
    }

    public String getK4Id() {
        return k4Id;
    }

    public String getK5Id() {
        return k5Id;
    }

    public String getK6Id() {
        return k6Id;
    }

    public String getK7Id() {
        return k7Id;
    }

    public String getkParkId() {
        return kParkId;
    }

    public String getkBMId() {
        return kBMId;
    }

    public String getkPrivilegesId() {
        return kPrivilegesId;
    }

    public String getkFranshizaId() {
        return kFranshizaId;
    }

    public String getNeedOTKId() {
        return needOTKId;
    }

    public Date getkOTKDate() {
        return kOTKDate;
    }

    public String getInputSecondName() {
        return inputSecondName;
    }

    public String getInputFirstName() {
        return inputFirstName;
    }

    public String getInputPatronymic() {
        return inputPatronymic;
    }

    public String getInputTelephone() {
        return inputTelephone;
    }

    public String getInputEmail() {
        return inputEmail;
    }

    public Boolean getIsprocessed(){
        return isProcessed;
    }

    public String getStatus(){
        return status;
    }

    public Timestamp getDateAdded() {
        return dateAdded;
    }
}