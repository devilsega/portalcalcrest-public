package ua.usstandart.osago.domainObject;

import java.sql.Timestamp;

/**
 * Объект для простого представления списка заказов ОСАГО
 */
public class OsagoCalcOrderSimpleForm {
    private Long id;
    private String FIO,status;
    private boolean isProcessed;
    private Timestamp dateAdded;

    public OsagoCalcOrderSimpleForm(Long id,
                                    String firstName, String secondName,String patronymic,String status,
                                    boolean isProcessed,
                                    Timestamp dateAdded){
        this.id = id;
        this.FIO = secondName+" "+firstName+" "+patronymic;
        this.status=status;
        this.isProcessed=isProcessed;
        this.dateAdded=dateAdded;
    }

    public Long getId() {
        return id;
    }

    public String getFIO(){
        return FIO;
    }

    public String getStatus() {
        return status;
    }

    public boolean isProcessed() {
        return isProcessed;
    }

    public Timestamp getDateAdded() {
        return dateAdded;
    }
}
