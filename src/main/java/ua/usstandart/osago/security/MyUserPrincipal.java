package ua.usstandart.osago.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ua.usstandart.osago.persistence.model.Role;
import ua.usstandart.osago.persistence.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * объект для аутентификации пользователя
 */

public class MyUserPrincipal implements UserDetails {
    private User user;

    public Collection<? extends GrantedAuthority> getAuthorities(){
        final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (final Role role: user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }

    public MyUserPrincipal(User user) {
        this.user = user;
    }

    public String getPassword(){
        return user.getPassword();
    }

    public String getUsername(){
        return user.getEmail();
    }

    public boolean isAccountNonExpired(){
        return true;
    }

    public boolean isAccountNonLocked(){
        return user.isEnabled();
    }

    public boolean isCredentialsNonExpired(){
        return true;
    }

    public boolean isEnabled(){
        return user.isEnabled();
    }

}
