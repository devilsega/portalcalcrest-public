package ua.usstandart.osago.enums;

/**
 * Все типы статусов заявки на ОСАГО
 */
public enum OsagoOrderStatus {
    NEW("new"),
    CLOSED_SUCCESSFULLY("closed successfully"),
    CLOSED_UNSUCCESSFULLY("closed unsuccessfully");

    private final String text;

    private OsagoOrderStatus(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

}