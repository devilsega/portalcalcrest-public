CREATE TABLE dbo.roles(
  id	bigserial PRIMARY KEY,
  name	varchar(20) NOT NULL,
  description	varchar(500) NOT NULL
);

INSERT INTO dbo.roles (name, description) VALUES
  ('ROLE_ADMIN','Role for siteadmin'),
  ('ROLE_MANAGER','Role for site manager'),
  ('ROLE_USER','Role for registered user');