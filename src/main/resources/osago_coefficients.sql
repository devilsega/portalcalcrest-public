﻿CREATE TABLE dbo.osago_coefficients(
coefficient_id	varchar(50) NOT NULL PRIMARY KEY,
coefficient_group	varchar(50) NOT NULL,
coefficient_value	FLOAT NOT NULL,
description	varchar(500) NOT NULL,
sorting_val int NOT NULL DEFAULT 1
);