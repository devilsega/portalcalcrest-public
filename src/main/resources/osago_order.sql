CREATE SEQUENCE dbo.osago_order_id_seq
INCREMENT 1
MINVALUE 1000
MAXVALUE 9223372036854775807
START 1000
CACHE 1;

CREATE TABLE dbo.osago_order(
  id bigint PRIMARY KEY NOT NULL DEFAULT nextval('dbo.osago_order_id_seq'::regclass),
  k1value FLOAT NOT NULL,
  k2value  FLOAT NOT NULL,
  k3value  FLOAT NOT NULL,
  k4value  FLOAT NOT NULL,
  k5value  FLOAT NOT NULL,
  k6value  FLOAT NOT NULL,
  k7value  FLOAT NOT NULL,
  kparkvalue FLOAT NOT NULL,
  kbmvalue FLOAT NOT NULL,
  kprivilegesvalue FLOAT NOT NULL,
  kfranshizavalue  FLOAT NOT NULL,
  needotkvalue FLOAT NOT NULL,
  calc_resultvalue FLOAT NOT NULL,
  k1id varchar(50) NOT NULL,
  k2id varchar(50) NOT NULL,
  k3id varchar(50) NOT NULL,
  k3aid  varchar(50) NOT NULL,
  k3bid  varchar(50) NOT NULL,
  k4id varchar(50) NOT NULL,
  k5id varchar(50) NOT NULL,
  k6id varchar(50) NOT NULL,
  k7id varchar(50) NOT NULL,
  kparkid  varchar(50) NOT NULL,
  kbmid  varchar(50) NOT NULL,
  kprivilegesid  varchar(50) NOT NULL,
  kfranshizaid varchar(50) NOT NULL,
  needotkid  varchar(50) NOT NULL,
  kotkdate date NOT NULL,
  inputsecondname  varchar(50) NOT NULL,
  inputfirstname varchar(50) NOT NULL,
  inputpatronymic  varchar(50) NOT NULL,
  inputtelephone varchar(50) NOT NULL,
  inputemail varchar(100) NOT NULL,
  isprocessed boolean NOT NULL,
  status varchar(100) NOT NULL,
  date_added timestamp with time zone NOT NULL
);
