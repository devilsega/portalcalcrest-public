CREATE TABLE dbo.users_roles (
  user_id BIGINT NOT NULL REFERENCES dbo.users(id),
  role_id BIGINT NOT NULL REFERENCES dbo.roles(id),
  PRIMARY KEY (user_id,role_id)
);
