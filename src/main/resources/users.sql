CREATE TABLE dbo.users(
  id bigserial PRIMARY KEY,
  email varchar(50) NOT NULL,
  password varchar(60) NOT NULL,
  date_added timestamp with time zone NOT NULL,
  enabled BOOLEAN DEFAULT FALSE
);