$(".submitChangeStatus").click(function () {
    var id = $("#id").text();
    var status = $("#changeStatus").val();
    var csrf = $("[name=_csrf]").val();
    var url = "/api/calculator/osago/1.0/private/manager/order/changestatus/"+id;
    $.ajax({
        url: url,
        dataType: "json",
        data:{
            id:id,
            status:status,
            _csrf:csrf
        },
        method: "POST",
        success: function() {
            location.reload(true);
        }
    });
});