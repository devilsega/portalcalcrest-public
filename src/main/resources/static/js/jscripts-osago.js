var receivedData;
var dataForCalc = new Object();
var dataForNewOrder = new Object();

$(document).on('mousedown', 'ul.typeahead', function(e) {
    e.preventDefault();
});

$(document).ready(function() {
    dataForCalc.k1 = "";
    dataForCalc.k2 = "";
    dataForCalc.k3 = "";
    dataForCalc.k3a = "";
    dataForCalc.k3b = "";
    dataForCalc.k4 = "";
    dataForCalc.k5 = "";
    dataForCalc.k6 = "";
    dataForCalc.k7 = "";
    dataForCalc.kPark = "";
    dataForCalc.kBM = "";
    dataForCalc.kPrivileges = "";
    dataForCalc.kFranshiza = "";
    dataForCalc.kOTK = "";

    $.ajax({
        url: "/api/calculator/osago/1.0/public/calculate",
        dataType: "json",
        method: "GET",
        data: dataForCalc,
        success: function(result) {
            receivedData = result;
            fillCalc();
            $("#transportB").click();
        }
    });
});

$("#transportB").click( function() {
    $('#k1').empty();
    for (var key in receivedData.k1) {
        if (key=="k1b1"||key=="k1b2"||key=="k1b3"||key=="k1b4"){
            $("#k1").append('<option value='+key+'>'+receivedData.k1[key]+'</option>');
        }
    }
    getCalcResult();
    $(":button").removeClass("buttonSelectAutoClassSelected");
    $(this).addClass("buttonSelectAutoClassSelected");
});

$("#transportC").click( function() {
    $('#k1').empty();
    for (var key in receivedData.k1) {
        if (key=="k1c1"||key=="k1c2"){
            $("#k1").append('<option value='+key+'>'+receivedData.k1[key]+'</option>');
        }
    }
    getCalcResult();
    $(":button").removeClass("buttonSelectAutoClassSelected");
    $(this).addClass("buttonSelectAutoClassSelected");
});

$("#transportD").click( function() {
    $('#k1').empty();
    for (var key in receivedData.k1) {
        if (key=="k1d1"||key=="k1d2"){
            $("#k1").append('<option value='+key+'>'+receivedData.k1[key]+'</option>');
        }
    }
    getCalcResult();
    $(":button").removeClass("buttonSelectAutoClassSelected");
    $(this).addClass("buttonSelectAutoClassSelected");
});

$("#transportF").click( function() {
    $('#k1').empty();
    for (var key in receivedData.k1) {
        if (key=="k1f1"||key=="k1e1"){
            $("#k1").append('<option value='+key+'>'+receivedData.k1[key]+'</option>');
        }
    }
    getCalcResult();
    $(":button").removeClass("buttonSelectAutoClassSelected");
    $(this).addClass("buttonSelectAutoClassSelected");
});

$("#transportA").click( function() {
    $('#k1').empty();
    for (var key in receivedData.k1) {
        if (key=="k1a1"||key=="k1a2"){
            $("#k1").append('<option value='+key+'>'+receivedData.k1[key]+'</option>');
        }
    }
    getCalcResult();
    $(":button").removeClass("buttonSelectAutoClassSelected");
    $(this).addClass("buttonSelectAutoClassSelected");
});

$(".custom-btn-block").change( function() {
    getCalcResult();
});

$(".fio").change( function() {
    var temp = $(this).val();
    var par = $($(this).parent());
    if (!validateCredentials(temp)){
        $(this).removeClass("form-valid").addClass("form-warning");
        if(par.find(".text-warning").length === 0){
            par.append('<p class="text-warning">Невірно заповнене поле!</p>');
        }
    }
    else{
        $(this).removeClass("form-warning").addClass("form-valid");
        par.find(".text-warning").remove();

    }
});

$("#inputTelephone").change( function() {
    var temp = $(this).val();
    var par = $($(this).parent());
    if (!validateTelephone(temp)){
        $(this).removeClass("form-valid").addClass("form-warning");
        if(par.find(".text-warning").length === 0){
            par.append('<p class="text-warning">Невірно заповнене поле!</p>');
        }
    }
    else{
        $(this).removeClass("form-warning").addClass("form-valid");
        par.find(".text-warning").remove();
    }
});

$("#inputEmail").change( function() {
    var temp = $(this).val();
    var par = $($(this).parent());
    if (!validateEMail(temp)){
        $(this).removeClass("form-valid").addClass("form-warning");
        if(par.find(".text-warning").length === 0){
            par.append('<p class="text-warning">Невірно заповнене поле!</p>');
        }
    }
    else{
        $(this).removeClass("form-warning").addClass("form-valid");
        par.find(".text-warning").remove();
    }
});

$(".buttonFinish").click( function() {
    sendToDB();
});

$(".buttonRetry").click( function() {
    location.reload(true);
});

$("#kOTK").change( function() {
    if($("#kOTK").val() === "kOTKYes"){
        $('#OTKDate').collapse("show");
    }
    if($("#kOTK").val() === "kOTKNo") {
        $('#OTKDate').collapse("hide");
    }
});

$('#datetimepickerOTK').datetimepicker({
    locale: 'uk',
    format: 'L',
    minDate: moment()
});

/*$(".collapse").on("shown.bs.collapse", function () {
    var ele = $(this);
    $('html, body').animate({
        scrollTop: $(ele).offset().top - 100
    }, 800);
});*/

/*
*************************************************************************************************
*/
function changeValues() {
    dataForCalc.k1 = $("#k1").val();
    dataForCalc.k2 = $("#k2").val();
    dataForCalc.k3 = selectFromObject(receivedData.k3,0);
    dataForCalc.k3a = $("#k3a").val();
    dataForCalc.k3b = $("#k3b").val();
    dataForCalc.k4 = selectFromObject(receivedData.k4,0);
    dataForCalc.k5 = selectFromObject(receivedData.k5,0);
    dataForCalc.k6 = selectFromObject(receivedData.k6,0);
    dataForCalc.k7 = $("#k7").val();
    dataForCalc.kPark = selectFromObject(receivedData.kPark,0);
    dataForCalc.kBM = $("#kBM").val();
    dataForCalc.kPrivileges = $("#kPrivileges").val();
    dataForCalc.kFranshiza = $("#kFranshiza").val();
    dataForCalc.kOTK = $("#kOTK").val();
}

function setDataForNewOrder() {
    dataForNewOrder.k1 = $("#k1").val();
    dataForNewOrder.k2 = $("#k2").val();
    dataForNewOrder.k3 = selectFromObject(receivedData.k3,0);
    dataForNewOrder.k3a = $("#k3a").val();
    dataForNewOrder.k3b = $("#k3b").val();
    dataForNewOrder.k4 = selectFromObject(receivedData.k4,0);
    dataForNewOrder.k5 = selectFromObject(receivedData.k5,0);
    dataForNewOrder.k6 = selectFromObject(receivedData.k6,0);
    dataForNewOrder.k7 = $("#k7").val();
    dataForNewOrder.kPark = selectFromObject(receivedData.kPark,0);
    dataForNewOrder.kBM = $("#kBM").val();
    dataForNewOrder.kPrivileges = $("#kPrivileges").val();
    dataForNewOrder.kFranshiza = $("#kFranshiza").val();
    dataForNewOrder.kOTK = $("#kOTK").val();
    dataForNewOrder.kOTKDate = selectKOTKDate();
    dataForNewOrder.inputSecondName = $("#inputSecondName").val();
    dataForNewOrder.inputFirstName = $("#inputFirstName").val();
    dataForNewOrder.inputPatronymic = $("#inputPatronymic").val();
    dataForNewOrder.inputTelephone = $("#inputTelephone").val();
    dataForNewOrder.inputEmail = $("#inputEmail").val();
}

function getCalcResult() {
    changeValues();
    if ($("#k1").val()!=="" && $("#k2").val()!==""){
        $.ajax({
            url: "/api/calculator/osago/1.0/public/calculate",
            dataType: "json",
            method: "GET",
            data: dataForCalc,
            success: function(result) {
                if (compareObjects(receivedData.k3a, result.k3a) && compareObjects(receivedData.k7, result.k7)
                    && compareObjects(receivedData.kPrivileges, result.kPrivileges) && compareObjects(receivedData.kBM, result.kBM)
                    && compareObjects(receivedData.kFranshiza, result.kFranshiza)
                    && compareObjects(receivedData.kOTK, result.kOTK)) {
                    receivedData = result;
                    $('#calcResult').empty().append(receivedData.result["result"]).append(" грн.");
                }
                else {
                    receivedData = result;
                    fillCalc();
                    getCalcResult();
                }
            }
        });
    }
}

function sendToDB() {
    if ($("#k1").val()!=="" && $("#k2").val()!==""&&$("#inputSecondName").hasClass("form-valid")&&$("#inputFirstName").hasClass("form-valid")&&$("#inputPatronymic").hasClass("form-valid")
        &&$("#inputTelephone").hasClass("form-valid")&&$("#inputEmail").hasClass("form-valid")){
        setDataForNewOrder();
        $.ajax({
            url: "api/calculator/osago/1.0/public/neworder",
            dataType: "json",
            method: "POST",
            data: dataForNewOrder,
            success: function(){
                $('#successModal').modal('show');
            },
            statusCode:{
                400: function () {
                    $('#wrongFormModal').modal('show');
                },
                500: function () {
                    alert("Внутришня помилка сервера, спробуйте пізніше.");
                },
                404: function () {
                    alert("Вибачте, але сервер тимчасово не доступний.");
                }
            }
        })
    }
    else {
        $('#wrongFormModal').modal('show');
    }
}

/*
*************************************************************************************************
*/

function compareObjects(a,b) {
    var lengthA = 0;
    var lengthB = 0;

    for(var item in a){
        lengthA++;
    }
    for(item in b){
        lengthB++;
    }

    if (lengthA!==lengthB)return false;

    for (item in a){
        if (item in b){
            continue;
        }
        else {
            return false;
        }
    }
    return true;
}

function fill(line,array,id) {
    if (line in array){
        for (var key in array) {
            if (key===line){
                $(id).append('<option value='+key+' selected>'+array[key]+'</option>');
            }
            else $(id).append('<option value='+key+'>'+array[key]+'</option>');
        }
    }
    else {
        var i = 0;
        for (var key in array) {
            if (i===0)$(id).append('<option value='+key+' selected>'+array[key]+'</option>');
            else $(id).append('<option value='+key+'>'+array[key]+'</option>');
            i++;
        }
    }
}

function fillKBM(selectedLine,array) {
    var validKBM = [];
    for (var key in array) {
        if (key ==="kBM3"||key ==="kBM4"|| key ==="kBM5" || key ==="kBM6" || key ==="kBM7"){
            validKBM.push(key);
        }
    }

    for (var validKey in validKBM){
        if (validKBM[validKey] === "kBM3")$("#kBM").append('<option value='+validKBM[validKey]+'>Менше року</option>');
        if (validKBM[validKey] === "kBM4")$('#kBM').append('<option value='+validKBM[validKey]+'>Один рік</option>');
        if (validKBM[validKey] === "kBM5")$('#kBM').append('<option value='+validKBM[validKey]+'>Два роки</option>');
        if (validKBM[validKey] === "kBM6")$('#kBM').append('<option value='+validKBM[validKey]+'>Три роки</option>');
        if (validKBM[validKey] === "kBM7")$('#kBM').append('<option value='+validKBM[validKey]+'>Чотири роки та більше</option>')
    }
    if($.inArray(selectedLine, validKBM) != -1) {
        $('#kBM option').filter(function(){
            return $(this).val() == selectedLine;
        }).prop("selected", true);
    }
}

function fillCalc() {
    var temp = $("#k3a").val();
    $('#k3a').empty();
    fill(temp,receivedData.k3a,"#k3a");

    temp = $("#k3b").val();
    $('#k3b').empty();
    fill(temp,receivedData.k3b,"#k3b");

    temp = $("#k7").val();
    $('#k7').empty();
    fill(temp,receivedData.k7,"#k7");

    temp = $("#kOTK").val();
    $('#kOTK').empty();
    fill(temp,receivedData.kOTK,"#kOTK");

    temp = $("#kPrivileges").val();
    $('#kPrivileges').empty();
    fill(temp,receivedData.kPrivileges,"#kPrivileges");

    temp = $("#kBM").val();
    $('#kBM').empty();
    fillKBM(temp,receivedData.kBM);

    temp = $("#kFranshiza").val();
    $('#kFranshiza').empty();
    fill(temp,receivedData.kFranshiza,"#kFranshiza");
}

function selectFromObject(data, index) {
    var str = [];
    for (var item in data){
        str.push(item);
    }
    return str[index];
}

function validateCredentials(input) {
    var regex = /^[a-zA-ZА-Яа-яёЁіІїЇ\-\'\s]{1,50}$/i;
    if (input.search(regex)===-1)return false;
    else return true;
}

function validateTelephone(input) {
    var regex = /^[\+\(\)\d\-\'\s]{1,50}$/i;
    if (input.search(regex)===-1)return false;
    else return true;
}

function validateEMail(input) {
    var regex = /.+@.+\..+/i;
    if (input.search(regex)===-1)return false;
    else return true;
}

function selectKOTKDate() {
    var OTKselect = $("#kOTK").val();
    var OTKDate =  $("#datetimepickerOTK").val();
    if (OTKselect === "kOTKNo"){
        var currentDate = moment().format('DD.MM.YYYY');
        return currentDate;
    }
    if(OTKselect === "kOTKYes"){
        return OTKDate;
    }
}