var cities = [];

$(document).ready(function() {
    $.getJSON('api/calculator/osago/1.0/public/getcities', function(result){
        cities = result;
    });

    var substringMatcher = function() {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];
            if (q === '') {
                matches.push("Київ","За межами України");
            }
            else {
                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp("^" + q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(cities, function (i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });
            }
            cb(matches);
        };
    };

    $('#k2').typeahead({
            hint: true,
            highlight: true,
            minLength: 0
        },
        {
            name: 'city',
            source: substringMatcher()
        }).on('typeahead:selected', function(){
        getCalcResult();
    });



});