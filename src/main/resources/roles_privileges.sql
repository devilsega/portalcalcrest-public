CREATE TABLE dbo.roles_privileges (
  role_id      BIGINT             NOT NULL REFERENCES dbo.roles (id),
  privilege_id BIGINT             NOT NULL REFERENCES dbo.privileges (id),
  PRIMARY KEY (role_id, privilege_id)
);

INSERT INTO dbo.roles_privileges (role_id, privilege_id) VALUES
  ('1', '1'),
  ('1', '2'),
  ('2','1');