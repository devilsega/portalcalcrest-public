CREATE TABLE dbo.privileges(
  id	bigserial PRIMARY KEY,
  name	varchar(20) NOT NULL
);

INSERT INTO dbo.privileges (name) VALUES
  ('READ_PRIVILEGE'),
  ('WRITE_PRIVILEGE');