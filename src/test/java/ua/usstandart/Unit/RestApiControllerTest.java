package ua.usstandart.Unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.usstandart.osago.config.SecurityConfig;
import ua.usstandart.osago.controller.RestApiController;
import ua.usstandart.osago.persistence.dao.RoleRepository;
import ua.usstandart.osago.persistence.dao.UserRepository;
import ua.usstandart.osago.security.AuthUserDetailsService;
import ua.usstandart.osago.service.impl.MailClient;
import ua.usstandart.osago.service.impl.OsagoCalcOrderServiceImpl;
import ua.usstandart.osago.service.impl.OsagoCalcServiceImpl;
import ua.usstandart.osago.service.impl.OsagoCalculator;
import ua.usstandart.osago.validator.OsagoValidator;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(RestApiController.class)
@Import(SecurityConfig.class)

public class RestApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    AuthUserDetailsService authUserDetailsService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private RoleRepository roleRepository;
    @MockBean
    private OsagoCalcServiceImpl osagoCalcService;
    @MockBean
    private OsagoCalcOrderServiceImpl osagoCalcOrderService;
    @MockBean
    private MailClient mailClient;
    @MockBean
    private OsagoCalculator osagoCalculator;
    @MockBean
    private OsagoValidator osagoFormValidator;

    @Test
    public void whenRequestCities_thenReturnJsonList() throws Exception {
        String[]cities = {"Київ"};

        given(osagoCalculator.getCities()).willReturn(cities);

        mvc.perform(get("/api/calculator/osago/1.0/public/getcities")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
