package ua.usstandart.Integration;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.usstandart.osago.service.impl.OsagoCalculator;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OsagoCalculatorIntegrationTest {
    //базовые значения для прогона теста
    private final String k1 = "k1b1";
    private final String k2 = "Київ";
    private final String k3a = "k3TaxiNo";
    private final String k3b = "k3Individual";
    private final String k4 = "k4Franshiza0RegardlessOfDrivingExp";
    private final String k5 ="k5EqualToContractTerm";
    private final String k6 = "k6FraudNo";
    private final String k7 = "k7OneYear";
    private final String kPark = "kParkTo4";
    private final String kBM = "kBM3";
    private final String kPrivileges = "kPrivilegesNo";
    private final String kFranshiza = "kFranshiza0uah";
    private final String kOTK = "kOTKNo";

    @Autowired
    private OsagoCalculator osagoCalculator;

    private void testCoefficients(String k1,String k2,String k3a,String k3b,String k4,String k5,String k6,String k7,String kPark,
                                  String kBM,String kPrivileges,String kFranshiza,String kOTK, String result){
        Map <String,Map<String,String>>  testCol = osagoCalculator.calculate(k1,k2,k3a,k3b,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK);
        assertThat(testCol).isNotNull();
        assertThat(testCol.get("result").get("result")).isEqualTo(result);
    }

    @Test
    public void getCities(){
        assertThat(osagoCalculator.getCities()).isNotNull();
    }

    @Test
    public void whenReceiveValidCoefficients_thenReturnValidResult(){
        Map <String,Map<String,String>>  testCol = osagoCalculator.calculate(k1,k2,k3a,k3b,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK);

        assertThat(testCol).isNotNull();
        assertThat(testCol.get("result").get("result")).isNotEqualTo("0,00");
    }

    @Test
    public void whenReceiveNonValidCoefficients_thenReturnZeroResultWithValidParams(){
        testCoefficients("","","","","","","","","","","","","","0,00");
    }

    @Test
    public void testK1(){
        String[]input = {"k1b1","k1b2","k1b3","k1b4","k1c1","k1c2","k1d1","k1d2","k1f1","k1e1","k1a1","k1a2"};
        String[]result = {"1520,64","1733,53","1794,36","2767,56","3041,28","3315,00","3877,63","4561,92","517,02","760,32","517,02","1034,04"};
        int i = 0;
        for(String item : input){
            testCoefficients(item,k2,k3a,k3b,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,result[i]);
            i++;
        }
    }

    @Test
    public void testK2(){
        String[]input = {"Київ","Бориспіль","Одеса","Запоріжжя","Вінниця","Бар","За межами України"};
        String[]result = {"1520,64","792,00","1077,12","887,04","696,96","475,20","950,40"};
        int i = 0;
        for(String item : input){
            testCoefficients(k1,item,k3a,k3b,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,result[i]);
            i++;
        }
    }

    @Ignore
    @Test
    public void testK3a(){
        //String[]inputK1 = {"k1b1","k1b2","k1b3","k1b4","k1c1","k1c2","k1d1","k1d2","k1f1","k1e1","k1a1","k1a2"};
        //String[]inputFransiza = {"kFranshiza2000uah","kFranshiza1000uah","kFranshiza500uah","kFranshiza0uah"};

        //if k1 = k1c1||k1c2||k1d2||k1f1||k1e1||k1a1||k1a2 or k2 = zone7, then k3a need to be "k3TaxiNo" only,
        //else if k1 = k1b1||k1b2||k1b3||k1b4||k1d1 and k2 != zone7, then k3a can bee k3TaxiNo and k3TaxiYes,
        //else result will be 0,00

        String[]inputK1 = {"k1b1","k1b2","k1b3","k1b4","k1d1"};
        String[]result = {"1520,64","1733,53","1794,36","2767,56","3877,63"};
        int i = 0;
        for(String item : inputK1){
            testCoefficients(item,k2,"k3TaxiNo",k3b,k4,k5,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,result[i]);
            i++;
        }

        //TODO: finish test
    }

    @Ignore
    @Test
    public void testK3b(){
        //TODO: finish test
    }

    @Test
    public void testK4(){
        //ignored in production build;
    }

    @Test
    public void testK5(){
        String[]input = {"k5EqualToContractTerm","k5SixMonths","k5SevenMonths","k5EightMonths","k5NineMonths","k5TenMonths","k5ElevenMonths"};
        String[]result = {"1520,64","1064,45","1140,48","1216,51","1292,54","1368,58","1444,61"};
        int i = 0;
        for(String item : input){
            testCoefficients(k1,k2,k3a,k3b,k4,item,k6,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,result[i]);
            i++;
        }
    }

    @Test
    public void testK6(){
        String[]input = {"k6FraudNo", "k6FraudYes"};
        String[]result = {"1520,64","3041,28"};
        int i = 0;
        for(String item : input){
            testCoefficients(k1,k2,k3a,k3b,k4,k5,item,k7,kPark,kBM,kPrivileges,kFranshiza,kOTK,result[i]);
            i++;
        }
    }

    @Test
    public void testK7(){
        String[]input = {"k7OneYear","k7ElevenMonths","k7TenMonths","k7NineMonths","k7EightMonths","k7SevenMonths","k7SixMonths","k7FiveMonths","k7FourMonths","k7ThreeMonths","k7TwoMonths","k7OneMonth","k7FifteenDays"};

        testCoefficients(k1,k2,k3a,k3b,k4,k5,k6,"k7OneYear",kPark,kBM,kPrivileges,kFranshiza,kOTK,"1520,64");

        String[]result = {"950,40","902,88","855,36","807,84","760,32","712,80","665,28","570,24","475,20","380,16","285,12","190,08","142,56"};
        int i = 0;
        for(String item : input){
            testCoefficients(k1,"За межами України",k3a,k3b,k4,k5,k6,item,kPark,kBM,kPrivileges,kFranshiza,kOTK,result[i]);
            i++;
        }

        input[0]=null;
        String[]inputK2 = {"Бориспіль","Одеса","Запоріжжя","Вінниця","Бар"};
        for(String item : input){
            for (String itemK2:inputK2) {
                testCoefficients(k1,itemK2,k3a,k3b,k4,k5,k6,item,kPark,kBM,kPrivileges,kFranshiza,kOTK,"0,00");
            }
        }
    }

    @Test
    public void testKPark(){
        String[]input = {"kParkTo4", "kPark5to9","kPark10to19","kPark20to99","kPark100to499","kPark500to1999","kPark2000AndMore"};
        String[]result = {"1520,64","1444,61","1368,58","1292,54","1216,51","1140,48","1064,45"};
        int i = 0;
        for(String item : input){
            testCoefficients(k1,k2,k3a,k3b,k4,k5,k6,k7,item,kBM,kPrivileges,kFranshiza,kOTK,result[i]);
            i++;
        }
    }

    @Test
    public void testkBM(){
        String[]input = {"kBMM", "kBM0","kBM1","kBM2","kBM3","kBM4","kBM5","kBM6","kBM7"};
        String[]result = {"3725,57","3497,47","2356,99","2128,90","1520,64","1444,61","1368,58","1292,54","1216,51"};
        int i = 0;
        for(String item : input){
            testCoefficients(k1,k2,k3a,k3b,k4,k5,k6,k7,kPark,item,kPrivileges,kFranshiza,kOTK,result[i]);
            i++;
        }
    }

    @Test
    public void testkPrivileges(){
        String[]input = {"kPrivilegesNo", "kPrivilegesYes"};
        String[]result = {"1520,64","760,32"};
        int i = 0;
        for(String item : input){
            testCoefficients(k1,k2,k3a,k3b,k4,k5,k6,k7,kPark,kBM,item,kFranshiza,kOTK,result[i]);
            i++;
        }
    }
}